use std::collections::BTreeMap;

use scheduler::{get_priority, set_policy, set_priority, Policy, Which};
use serde::Deserialize;

use crate::cgroup::Cgroup;
use crate::sys::io;

#[derive(Clone, Debug, Eq, PartialEq, Deserialize)]
//#[serde(tag = "type", content = "args")]
pub enum Sched {
    #[serde(rename(deserialize = "normal"))]
    Other,
    #[serde(rename(deserialize = "fifo"))]
    Fifo(u8),
    #[serde(rename(deserialize = "rr"))]
    RoundRobin(u8),
    #[serde(rename(deserialize = "batch"))]
    Batch,
    #[serde(rename(deserialize = "idle"))]
    Idle,
    #[serde(rename(deserialize = "deadline"))]
    Deadline,
}

#[derive(Clone, Deserialize, Default, Debug)]
pub struct Type {
    nice: Option<i8>,

    ioclass: Option<io::IoClass>,

    cgroup: Option<String>,
    sched: Option<Sched>,
}

impl Type {
    pub fn apply(&self, pid: i32, cgroups: &BTreeMap<String, Cgroup>) {
        self.apply_nice(pid);
        self.apply_scheduler(pid);
        self.apply_ioclass(pid);
        self.apply_cgroup(pid, cgroups);
    }

    fn apply_nice(&self, pid: i32) {
        if let Some(nice) = self.nice {
            match set_priority(Which::Process, pid, nice as i32) {
                Ok(()) => {}
                Err(()) => {
                    error!("error setting nice {} for {}", nice, pid);
                }
            };
        }
    }

    fn apply_scheduler(&self, pid: i32) {
        if let Some(sched) = &self.sched {
            let mut priority = 0;
            let policy = match sched {
                Sched::Other => Policy::Other,
                Sched::Fifo(pro) => {
                    priority = *pro;
                    Policy::Fifo
                }
                Sched::RoundRobin(pro) => {
                    priority = *pro;
                    Policy::RoundRobin
                }
                Sched::Batch => Policy::Batch,
                Sched::Idle => Policy::Idle,
                Sched::Deadline => Policy::Deadline,
            };
            match set_policy(pid, policy, priority as i32) {
                Ok(()) => {}
                Err(()) => {
                    error!("error setting sched policy {:?} for {}", sched, pid);
                }
            };
        }
    }

    fn apply_ioclass(&self, pid: i32) {
        if let Some(ioclass) = &self.ioclass {
            match io::set_class(pid, ioclass) {
                Ok(()) => {}
                Err(err) => {
                    error!("error setting ioclass {:?} for {}, error: {}", ioclass, pid, err);
                }
            };
        }
    }

    fn apply_cgroup(&self, pid: i32, cgroups: &BTreeMap<String, Cgroup>) {
        if let Some(cgroup) = &self.cgroup {
            info!("adding pid: {} to cgroup", pid);
            match cgroups.get(cgroup) {
                Some(cgroup) => {
                    if let Err(err) = cgroup.attach_pid(pid) {
                        error!("error adding pid: {} to cgroup: {:?}", pid, err);
                    }
                }
                None => error!("cgroup {} not found!", cgroup),
            };
        }
    }
}
