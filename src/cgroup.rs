use std::error::Error;

use cgroups_rs::{cgroup_builder::CgroupBuilder, Cgroup as CgroupHolder, CgroupPid};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct Cpu {
    pub quota: u8,
}

impl Cpu {
    pub fn init(&self, period_us: u64) -> (u64, u64) {
        let quota_us: u64 = period_us * num_cpus::get() as u64 * self.quota as u64 / 100;
        let shares: u64 = 1024 * self.quota as u64 / 100;
        //ctrl.add_u64("cpu.cfs_period_us", period_us)?;
        //ctrl.add_u64("cpu.cfs_quota_us", cfs_quota_us)?;
        //ctrl.add_u64("cpu.shares", shares)?;
        (quota_us, shares)
    }
}

#[derive(Debug)]
struct CgroupInterface {
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct Cgroup {
    cpu: Option<Cpu>,

    #[serde(skip)]
    name: String,
    #[serde(skip)]
    period_us: u64,
    #[serde(skip)]
    cg: Option<CgroupHolder>,
}

impl Cgroup {
    pub fn init(&mut self, name: &str, period_us: u64) -> Result<(), Box<dyn Error>> {
        self.period_us = period_us;
        self.name = name.to_string();
        self.create()?;
        Ok(())
    }

    fn create(&mut self) -> Result<(), Box<dyn Error>> {
        let hier = cgroups_rs::hierarchies::auto();
        let mut cg = CgroupBuilder::new(&self.name);

        if let Some(cpu) = &self.cpu {
            let cpu = cpu.init(self.period_us);

            info!("setting cgroup: {} cpu quota: {} shares: {}", self.name, cpu.0, cpu.1);

            cg = cg.cpu().quota(cpu.0 as i64).shares(cpu.1).done();
        }

        self.cg = Some(cg.build(hier));
        Ok(())
    }

    pub fn attach_pid(&self, pid: i32) -> Result<(), Box<dyn Error>> {
        if let Some(cg) = &self.cg {
            cg.add_task(CgroupPid::from(pid as u64))?;
        }
        Ok(())
    }
}
