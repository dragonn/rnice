use std::collections::{BTreeMap, HashMap, HashSet};
use std::{thread, time};

use procfs::process::Process as Prc;

use crate::cgroup::Cgroup;
use crate::config::Config;
use crate::globals;
use crate::pid::Pid;

#[derive(Debug)]
struct NamedPids(HashMap<i32, Pid>);

impl NamedPids {
    pub fn new(key: i32, pid: Pid) -> Self {
        let mut pids = HashMap::with_capacity(1);
        pids.insert(key, pid);
        Self(pids)
    }

    pub fn get_mut(&mut self, key: &i32) -> Option<&mut Pid> {
        self.0.get_mut(key)
    }

    pub fn insert(&mut self, key: i32, pid: Pid) {
        self.0.insert(key, pid);
    }

    pub fn retain<F: FnMut(&i32, &mut Pid) -> bool>(&mut self, f: F) {
        self.0.retain(f);
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn cpu_usage(&self) -> f64 {
        self.0.iter().map(|(_, v)| v.cpu_usage()).sum()
    }

    pub fn throttle_group(&mut self, state: bool, cgroups: &BTreeMap<String, Cgroup>) {
        self.0.iter_mut().for_each(|(_, pid)| pid.check_throttle_grouped(state, cgroups));
    }
}

#[derive(Debug)]
pub struct Daemon {
    config: Config,
    pids: HashMap<String, NamedPids>,
}

impl Daemon {
    pub fn new(config: Config) -> Self {
        Self { config, pids: HashMap::new() }
    }

    pub fn update_process_stats(&mut self, processes: &Vec<Prc>) {
        let mut current_pids: HashSet<i32> = HashSet::with_capacity(processes.len());
        for prc in processes {
            current_pids.insert(prc.pid);
            match self.pids.get_mut(&prc.stat.comm) {
                Some(pids) => {
                    match pids.get_mut(&prc.pid) {
                        Some(pid) => {
                            if let Some(_throttle) = &self.config.throttle {
                                pid.update(&prc);
                                pid.apply_type(&self.config.cgroups);
                            }
                        }
                        None => {
                            pids.insert(prc.pid, Pid::new(&prc, &self.config));
                        }
                    };
                }
                None => {
                    self.pids.insert(prc.stat.comm.clone(), NamedPids::new(prc.pid, Pid::new(&prc, &self.config)));
                }
            };
        }
        for (comm, pids) in self.pids.iter_mut() {
            pids.retain(|key, _| current_pids.contains(&key));
            if let Some(throttle) = &self.config.throttle {
                if let Some(rule) = throttle.get_process_rule(&comm) {
                    if let Some(grouped_cpu_usage) = rule.grouped_cpu_usage {
                        let cpu_usage = pids.cpu_usage();
                        pids.throttle_group(cpu_usage > grouped_cpu_usage as f64, &self.config.cgroups);
                    }
                }
            }
        }

        self.pids.retain(|_, pids| pids.len() > 0);
    }

    pub fn run(&mut self) {
        let interval_ms = time::Duration::from_millis(self.config.interval);
        globals::set_interval_ms(self.config.interval);

        loop {
            let processes = procfs::process::all_processes().unwrap();
            self.update_process_stats(&processes);
            thread::sleep(interval_ms);
        }
    }
}
