use std::collections::{BTreeMap, HashMap};
use std::fs::{create_dir, File};
use std::io::prelude::*;
use std::path::Path;

use config;
use serde::Deserialize;

use crate::cgroup::Cgroup;
use crate::r#type::Type;
use crate::throttle::Throttle;

#[derive(Deserialize, Default, Debug)]
pub struct Config {
    pub interval: u64,
    pub period_us: u64,
    pub throttle: Option<Throttle>,
    pub types: BTreeMap<String, Type>,
    pub processes: BTreeMap<String, String>,
    pub cgroups: BTreeMap<String, Cgroup>,
}

impl Config {
    #[cfg(debug_assertions)]
    pub fn new() -> Self {
        let mut config = config::Config::default();

        config.merge(config::File::with_name("configs/Settings")).expect("missing settings");
        config.merge(config::File::with_name("configs/Types")).expect("missing types");
        if let Err(_) = config.merge(config::File::with_name("configs/Throttle")) {
            warn!("throotle module disabled");
        }
        config.merge(config::File::with_name("configs/Processes")).expect("missing processes");
        config.merge(config::File::with_name("configs/Cgroups")).expect("missing cgroups");

        config.try_into::<Self>().unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn check() {
        if !Path::new("/usr/lib/systemd/system/rnice.service").exists() {
            let service = include_bytes!("../rnice.service");
            let mut file = File::create("/usr/lib/systemd/system/rnice.service").expect("error creating service");
            file.write_all(service).expect("error writing service");

            info!("installed systemd service rnice.service");
            info!("enable it with: systemctl enable rnice.service");
        }

        if !Path::new("/etc/rnice").exists() {
            create_dir("/etc/rnice").expect("error creating config directory /etc/rnice");

            info!("created config directory /etc/rnice");
        }

        if !Path::new("/etc/rnice/Settings.toml").exists() {
            let settings = include_bytes!("../configs/Settings.toml");
            let mut file = File::create("/etc/rnice/Settings.toml").expect("error creating default Settings.toml");
            file.write_all(settings).expect("error writing default Settings.toml");

            info!("created Settings.toml in /etc/rnice");
        }

        if !Path::new("/etc/rnice/Types.toml").exists() {
            let types = include_bytes!("../configs/Types.toml");
            let mut file = File::create("/etc/rnice/Types.toml").expect("error creating default Types.toml");
            file.write_all(types).expect("error writing default Types.toml");

            info!("created Types.toml in /etc/rnice");
        }

        if !Path::new("/etc/rnice/Throttle.toml").exists() {
            let throttle = include_bytes!("../configs/Throttle.toml");
            let mut file = File::create("/etc/rnice/Throttle.toml").expect("error creating default Throttle.toml");
            file.write_all(throttle).expect("error writing default Throttle.toml");

            info!("created Throttle.toml in /etc/rnice");
        }

        if !Path::new("/etc/rnice/Processes.toml").exists() {
            let processes = include_bytes!("../configs/Processes.toml");
            let mut file = File::create("/etc/rnice/Processes.toml").expect("error creating default Processes.toml");
            file.write_all(processes).expect("error writing default Processes.toml");

            info!("created Processes.toml in /etc/rnice");
        }

        if !Path::new("/etc/rnice/Cgroups.toml").exists() {
            let cgroups = include_bytes!("../configs/Cgroups.toml");
            let mut file = File::create("/etc/rnice/Processes.toml").expect("error creating default Cgroups.toml");
            file.write_all(cgroups).expect("error writing default Cgroups.toml");

            info!("created Cgroups.toml in /etc/rnice");
        }
    }

    #[cfg(not(debug_assertions))]
    pub fn new() -> Self {
        Self::check();
        let mut config = config::Config::default();

        config.merge(config::File::with_name("/etc/rnice/Settings")).expect("missing settings");
        config.merge(config::File::with_name("/etc/rnice/Types")).expect("missing types");
        if let Err(_) = config.merge(config::File::with_name("/etc/rnice/Throttle")) {
            warn!("throotle module disabled");
        }
        config.merge(config::File::with_name("/etc/rnice/Processes")).expect("missing processes");
        config.merge(config::File::with_name("/etc/rnice/Cgroups")).expect("missing cgroups");

        config.try_into::<Self>().unwrap()
    }
}
