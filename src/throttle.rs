use procfs::process::Process as Prc;
use std::collections::BTreeMap;
use std::time::SystemTime;

use serde::Deserialize;

use crate::globals;
use crate::pid::Pid;

#[derive(Deserialize, Clone, Default, Debug)]
pub struct Rule {
    pub cpu_usage: Option<u16>,
    pub grouped_cpu_usage: Option<u16>,
    pub time: u16,
    pub r#type: String,
    #[serde(default)]
    pub group: bool,
}

#[derive(Deserialize, Default, Debug)]
pub struct Throttle {
    all: Option<Rule>,
    rules: BTreeMap<String, Rule>,
}

impl Throttle {
    pub fn get_process_rule(&self, comm: &str) -> Option<Rule> {
        let mut rule = self.rules.get(comm).cloned();
        if let None = rule {
            rule = self.all.clone();
        }

        rule
    }
}
