use procfs::process::Process as Prc;
use std::collections::BTreeMap;
use std::time::SystemTime;

use crate::cgroup::Cgroup;
use crate::config::Config;
use crate::globals;
use crate::r#type::Type;
use crate::throttle::Rule;

#[derive(Default, Debug)]
pub struct Pid {
    pub pid: i32,
    pub comm: String,

    curr_time: u64,
    prev_time: Option<u64>,

    r#type: Option<Type>,

    throttle: bool,
    throttle_grouped: bool,
    throttle_rule: Option<Rule>,
    throttle_type: Option<Type>,
    throttle_start: Option<SystemTime>,
}

impl Pid {
    pub fn new(prc: &Prc, config: &Config) -> Self {
        let mut throttle_rule = None;
        let mut throttle_type = None;
        if let Some(throttle) = &config.throttle {
            throttle_rule = throttle.get_process_rule(&prc.stat.comm);
            throttle_type = if let Some(throttle_rule) = &throttle_rule { config.types.get(&throttle_rule.r#type).cloned() } else { None };
        }

        let r#type = if let Some(r#type) = config.processes.get(&prc.stat.comm) { config.types.get(r#type).cloned() } else { None };
        //info!("new process: {}", prc.stat.comm);

        let pid = Self {
            pid: prc.pid,
            comm: prc.stat.comm.clone(),

            curr_time: prc.stat.utime + prc.stat.stime,
            prev_time: None,

            r#type,

            throttle: false,
            throttle_grouped: false,
            throttle_rule,
            throttle_type,
            throttle_start: None,
        };

        pid.init_type(&pid.r#type, &config.cgroups);
        pid
    }

    pub fn update(&mut self, prc: &Prc) {
        self.prev_time = Some(self.curr_time);
        self.curr_time = prc.stat.utime + prc.stat.stime;
    }

    pub fn cpu_usage(&self) -> f64 {
        if let Some(prev_time) = self.prev_time {
            let usage_ms = (self.curr_time - prev_time) as f64 * 1000 as f64 / globals::get_ticks() as f64;
            let usage = usage_ms as f64 * 100.0 / globals::get_interval_ms() as f64;

            usage
        } else {
            0.0
        }
    }

    pub fn check_throotle(&mut self) -> bool {
        if let Some(rule) = &self.throttle_rule {
            let cpu_usage = self.cpu_usage();
            if let Some(rule_cpu_usage) = rule.cpu_usage {
                if cpu_usage <= rule_cpu_usage as f64 {
                    self.throttle = false;
                    self.throttle_start = None;
                } else {
                    if let Some(over_cpu_usage_start) = self.throttle_start {
                        if over_cpu_usage_start.elapsed().unwrap().as_secs() >= rule.time as u64 {
                            self.throttle = true;
                            return true;
                        }
                    } else {
                        self.throttle = false;
                        self.throttle_start = Some(SystemTime::now());
                    }
                }
            }
        }

        false
    }

    fn init_type(&self, r#type: &Option<Type>, cgroups: &BTreeMap<String, Cgroup>) {
        if let Some(r#type) = r#type {
            //debug!("applayin type:  {:?} to pid: {:?}", r#type, self);
            r#type.apply(self.pid, cgroups);
        }
    }

    pub fn apply_type(&mut self, cgroups: &BTreeMap<String, Cgroup>) {
        let throttle_old = self.throttle;
        let throotle_new = self.check_throotle();
        if throotle_new != throttle_old {
            if throotle_new {
                info!("throotling: {:?}", self);
                self.init_type(&self.throttle_type, cgroups);
            } else {
                self.init_type(&self.r#type, cgroups);
            }
        }
    }

    pub fn check_throttle_grouped(&mut self, throttle_grouped_new: bool, cgroups: &BTreeMap<String, Cgroup>) {
        let throttle_grouped_old = self.throttle_grouped;
        if throttle_grouped_old != throttle_grouped_new {
            if throttle_grouped_new {
                info!("grouped throotling: {:?}", self);
                self.init_type(&self.throttle_type, cgroups);
            } else {
                self.init_type(&self.r#type, cgroups);
            }
            self.throttle_grouped = throttle_grouped_new;
        }
    }
}
