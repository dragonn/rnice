use std::cell::RefCell;
use std::ops::Deref;

thread_local! {
    static INTERVAL_MS: RefCell<u64> = RefCell::new(0);
    static TICKS: RefCell<i64> = RefCell::new(procfs::ticks_per_second().unwrap_or(100));
}

pub fn set_interval_ms(val: u64) {
    INTERVAL_MS.with(|refcell| { refcell.replace(val); })
}

pub fn get_ticks() -> i64 {
    TICKS.with(|refcell| *refcell.borrow().deref())
}


pub fn get_interval_ms() -> u64 {
    INTERVAL_MS.with(|refcell| *refcell.borrow().deref())
}