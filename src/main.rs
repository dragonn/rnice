#![feature(in_band_lifetimes)]

#[macro_use]
extern crate log;

use std::{thread, time};

mod cgroup;
mod config;
mod daemon;
mod globals;
mod pid;
mod sys;
mod throttle;
mod r#type;

fn main() {
    std::env::set_var("RUST_LOG", "debug,libcgroup_rs=error");
    std::env::set_var("RUST_BACKTRACE", "1");

    env_logger::init();

    //let hier = cgroups_rs::hierarchies::auto();

    info!("starting rnice!");
    let mut config: config::Config = config::Config::new();

    for (key, value) in config.cgroups.iter_mut() {
        if let Err(err) = value.init(key, config.period_us) {
            error!("error creating cgroup: {} err: {}", &key, err);
        }
    }

    //debug!("config: {:?}", config);

    let interval = time::Duration::from_millis(config.interval);
    let interval_ms = interval.as_secs() * 1000 + u64::from(interval.subsec_millis());
    globals::set_interval_ms(interval_ms);

    let mut daemon = daemon::Daemon::new(config);
    daemon.run();
}
