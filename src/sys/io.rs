//based on https://github.com/karelzak/util-linux/blob/master/schedutils/ionice.c

use serde::Deserialize;


use libc::{c_int, syscall, SYS_ioprio_set};

const IOPRIO_CLASS_SHIFT: c_int = 13;
const IOPRIO_WHO_PROCESS: c_int = 1;

#[derive(Clone, Debug, Eq, PartialEq, Deserialize)]
#[repr(C)]
pub enum IoClass {
    #[serde(rename(deserialize="none"))] r#None,
    #[serde(rename(deserialize="realtime"))] RealTime(u8),
    #[serde(rename(deserialize="best-effort"))] BestEffort(u8),
    #[serde(rename(deserialize="idle"))] Idle
}

pub fn set_class(pid: i32, policy: &IoClass) -> Result<(), i64> {
    let mut c_data: c_int = 4;
    let c_class: c_int = match policy {
        IoClass::None => { c_data = 0; 0 },
        IoClass::RealTime(data) => { c_data = *data as i32; 1 },
        IoClass::BestEffort(data) => { c_data = *data as i32; 2 },
        IoClass::Idle => { c_data = 7; 3 },
    };
    
    let c_ioprio: c_int = (c_class << IOPRIO_CLASS_SHIFT) | c_data;

    match unsafe { syscall(SYS_ioprio_set, IOPRIO_WHO_PROCESS, pid, c_ioprio) } {
        0 => Ok(()),
        err => Err(err),
    }
}